//
//  SecondViewController.swift
//  MemoryLeakDemo
//
//  Created by st29 on 29/10/16.
//  Copyright © 2016 Surekha Technologies. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var selectedImage           : UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
}


extension SecondViewController : UITableViewDelegate , UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let imageName = indexPath.row % 6
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        cell?.imageView?.image = UIImage.init(named: "\(imageName).jpg")
        cell?.textLabel?.text = "Image:  \(indexPath.row+1)"
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let imageName       = indexPath.row % 6
        self.selectedImage  = UIImage.init(named: "\(imageName).jpg")
        let detailViewController = self.storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        detailViewController.image = self.selectedImage
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  60;
    }
}

