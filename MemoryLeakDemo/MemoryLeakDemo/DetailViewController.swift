//
//  DetailViewController.swift
//  MemoryLeakDemo
//
//  Created by st29 on 29/10/16.
//  Copyright © 2016 Surekha Technologies. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet var imageView: CustomImageView!
    
    var data            : NSData?
    var image           : UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.imageView.delegate = self
        self.imageView.image    = self.image
        
        let filePath = Bundle.main.path(forResource: "Test", ofType: "txt")
        data = NSData(contentsOfFile: filePath!);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
